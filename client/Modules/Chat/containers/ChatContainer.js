import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import io from 'socket.io-client'

import ChatBoxContainer from './ChatBoxContainer'
import ChannelContainer from './ChannelContainer'

class ChatContainer extends Component {
  constructor(props) {
    super(props)
    this.socket = io('', { path: '/chat' })
  }

  render() {
    return (
      <div className="columns" style={{marginTop: '20px'}}>
        <div className="column is-two-thirds">
          <ChatBoxContainer socket={this.socket}/>
        </div>
        <div className="column">
          <ChannelContainer socket={this.socket}/>
        </div>
      </div>
    )
  }
}

export default ChatContainer
