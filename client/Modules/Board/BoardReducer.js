import { START_GAME, SET_NAME, MAKE_MOVE, MAKE_ANSWER, CHOOSE_GAME_TYPE, REQUEST_GAME, CREATE_GAME, RECEIVE_GAME } from './BoardActions'
import cookie from 'react-cookie'

export const WITH_AI = 1
export const WITH_PLAYER = 2

const initialState = {
  isLoaded: false,
  isLoading: false,
  game: {
    board: [],
    player1: '',
    player2: ''
  },
  isNextTurn: true,
  value: 0
}

const BoardReducer = (state = initialState, action) => {
  switch(action.type) {
    case SET_NAME:
      return Object.assign({}, state, {username: action.username})
    case CREATE_GAME:
    case REQUEST_GAME:
      return Object.assign({}, state, {
        isLoading: true
      })
    case RECEIVE_GAME:
      return Object.assign({}, state, {
        isLoaded: true,
        isLoading: false,
        isPlaying: true,
        game: action.game,
        value: action.value
      })
    case START_GAME:
      return Object.assign({}, state, {playing: !state.playing})
    case MAKE_MOVE: {
      let board = []
      state.game.board.map(row => board.push(Array.from(row)))
      board[action.pos.x][action.pos.y] = action.value

      return Object.assign({}, state, {
        game: Object.assign({}, state.game, {board: board}),
        lastMove: {
          x: action.pos.x,
          y: action.pos.y
        },
        isNextTurn: action.isNextTurn
      })
    }
    case MAKE_ANSWER:
      return Object.assign({}, state, {
        isNextTurn: true
      })
    case CHOOSE_GAME_TYPE:
      return Object.assign({}, state, {gameType: action.typeId})
    default:
      return state
  }
}

export default BoardReducer
