import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import io from 'socket.io-client'
import cookie from 'react-cookie'

import { startGame, setName, makeMoveRequest, makeMove, makeAnswer, fetchGame, createGameRequest } from '../BoardActions'
import { WITH_AI, WITH_PLAYER } from '../BoardReducer'
import Board from '../components/Board'
import Gomoku from '../Gomoku'

class BoardContainer extends Component {
  constructor(props) {
    super(props)
    this.socket = props.socket
    this.state = {
      username: cookie.load('username') || ''
    }
    this.gomoku = new Gomoku(props.size, -1)
    this.handleMouseDown = this.handleMouseDown.bind(this)
  }

  componentDidMount = () => {
    const { game, isPlaying, username, currentChannel, dispatch } = this.props

    this.socket.emit('connect lobby', this.state.username)

    this.socket.on('answer connect lobby', (username) => {

      this.setState({
        username: username.replace('/#', '')
      })
      console.log(this.state.username)
      this.socket.emit('room members', {
        channel: currentChannel
      })
    })

    this.socket.on('answer room members', (data) => {
      if(data.members >= 2) {
        dispatch(fetchGame(currentChannel))
      } else {
        dispatch(createGameRequest(this.state.username, currentChannel))
      }
    })

    this.socket.on('make move bc', (data) => {
      dispatch(makeMove(data.pos, true, data.value))
    })
  }

  handleResetGame = () => {
    this.setState({
      matrix: this.initalizeBoardMatrix()
    })
  }

  handleMouseDown = (e) => {
    const { game, currentChannel, isNextTurn, value, dispatch } = this.props
    const [x, y] = e.target.id.split('-')

    if (x && y && game.board[x][y] == 0 && isNextTurn) {
      this.socket.emit('make move', {
          channel: currentChannel,
          pos: {
            x,
            y
          },
          value
      })
      dispatch(makeMoveRequest({
        id: game._id,
        oldBoard: game.board,
        pos: {
          x,
          y
        },
        value
      }))
    }
  }

  componentDidUpdate = (prevProps, prevState) => {
    // console.log(prevProps)
  }

  componentWillReceiveProps = (nextProps) => {
    const { boardMatrix, gameType, lastMove, isNextTurn, dispatch } = nextProps
    if(!isNextTurn) {
      switch(gameType) {
        case WITH_AI:
          dispatch(makeAnswer(this.gomoku.getBestMove(boardMatrix, lastMove)))
          break
        case WITH_PLAYER:
          break
      }
    }
    const winner = this.gomoku.checkWin(boardMatrix)
  }

  render() {
    const { game } = this.props
    return (
      <Board game={game}
        handleMouseDown={this.handleMouseDown}
        />
    )
  }
}
BoardContainer.propTypes = {
  socket: PropTypes.object.isRequired
}
const mapStateToProps = (state) => {
  return {
    currentChannel: state.channel.currentChannel,
    game: state.board.game,
    username: state.board.username,
    isNextTurn: state.board.isNextTurn,
    value: state.board.value
  }
}

export default connect(mapStateToProps)(BoardContainer)
