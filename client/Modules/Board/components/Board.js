import React, { Component, PropTypes } from 'react'
import styles from './styles/board.scss'

const Board = ({ game, handleMouseDown }) => {
  const boardRows = []
  const { board } = game
  board.map((row, i) => {
    boardRows.push(
      <div className={styles.boardRow}> {
        row.map((cell, j) => {
          return (
            <div className={styles.boardCol} onMouseDown={handleMouseDown} key={`${i}-${j}`} id={`${i}-${j}`} >
              <div className={
                (cell == 1) ? styles.boardCellCross :
                (cell == -1) ? styles.boardCellCircle : null}></div>
            </div>
          )
        })
      } </div>
    )
  })

  return (
    <div>
      <p className="control has-addons">
        <input className="input" type="text" />
        <a className="button is-info"> Change Name</a>
      </p>
      <div className={styles.board}>
        {boardRows}
      </div>
    </div>
  )
}

export default Board
