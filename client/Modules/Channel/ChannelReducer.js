import { REQUEST_CHANNELS, RECEIVE_CHANNELS, ADD_CHANNEL, CHANGE_CHANNEL } from './ChannelActions'

const initialState = {
  isLoaded: false,
  isLoading: false,
  isPlaying: false,
  currentChannel: 'Lobby',
  channels: [{ name: 'Lobby' }],
}

const ChannelReducer = (state = initialState, action) => {
  switch(action.type) {
    case REQUEST_CHANNELS:
      return Object.assign({}, state, {
        isLoading: true
      })
    case RECEIVE_CHANNELS:
      return Object.assign({}, state, {
        isLoaded: true,
        isLoading: false,
        channels: [...state.channels, ...action.channels]
      })
    case ADD_CHANNEL:
      return Object.assign({}, state, {
        currentChannel: action.channel.name,
        channels: [...state.channels, action.channel]
      })
    case CHANGE_CHANNEL:
      return Object.assign({}, state, {
        currentChannel: action.channel
      })
    default: return state
  }
}

export default ChannelReducer
