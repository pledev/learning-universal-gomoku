import Message from '../models/message';

export function getMessages(req, res) {

  let condition = (req.params.channel) ? { channel: req.params.channel } : {}
  Message.find(condition).exec((err, messages) => {
    if (err) {
      res.status(500).send(err);
    }
    res.json({ messages });
  });
}

export function addMessage(req, res) {
  if(!req.body.message.username || !req.body.message.channel || !req.body.message.text) {
    res.status(403).end();
  }

  const newMessage = new Message(req.body.message);

  newMessage.save((err, saved) => {
    if (err) {
      res.status(500).send(err);
    }
    res.json({ message: saved });
  });
}
