import callApi from '../../../common/util'

export const ADD_MESSAGE = 'ADD_MESSAGE'
export const ADD_MESSAGE_REQUEST = 'ADD_MESSAGE_REQUEST'
export const RECEIVE_MESSAGE = 'RECEIVE_MESSAGE'
export const REQUEST_MESSAGES = 'REQUEST_MESSAGES'
export const RECEIVE_MESSAGES = 'RECEIVE_MESSAGES'
export const TYPING = 'TYPING'
export const STOP_TYPING = 'STOP_TYPING'

const addMessage = (message) => {
  return {
    type: ADD_MESSAGE,
    message
  };
}

export const addMessageRequest = (message) => {
  return (dispatch) => {
    return callApi('messages', 'post', {
      message: message
    }).then(res => dispatch(addMessage(res.message)))
  }
}

export const receiveRawMessage = (message) => {
  return {
    type: RECEIVE_MESSAGE,
    message
  };
}
export function typing(typingUser) {
  return {
    type: TYPING,
    typingUser
  };
}

export function stopTyping(typingUser) {
  return {
    type: STOP_TYPING,
    typingUser
  };
}
const requestMessages = () => {
  return {
    type: REQUEST_MESSAGES
  }
}
const receiveMessages = (messages) => {
  return {
    type: RECEIVE_MESSAGES,
    messages
  }
}

export const fetchMessages = (channel) => {
  return dispatch => {
    dispatch(requestMessages())
    return callApi(`messages/${channel}`).then(res => {
      dispatch(receiveMessages(res.messages))
    }).catch(error => {throw error})
  }
}
