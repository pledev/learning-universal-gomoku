import { REQUEST_MESSAGES, RECEIVE_MESSAGES, ADD_MESSAGE, TYPING, STOP_TYPING } from './ChatBoxActions'

const initialState = {
  loaded: false,
  loading: false,
  messages: [],
  typingUsers: []
}

const MessageReducer = (state = initialState, action) => {
  switch(action.type) {
    case REQUEST_MESSAGES:
      return Object.assign({}, state, { loading: true })
    case RECEIVE_MESSAGES:
      return Object.assign({}, state, {
        loaded: true,
        loading: false,
        messages: [...action.messages]
      })
    case ADD_MESSAGE:
      return Object.assign({}, state, {
        messages: [...state.messages, action.message]
      })
    case TYPING:
      return Object.assign({}, state, {
        typingUsers: [...state.typingUsers, action.typingUser]
      })
    case STOP_TYPING:
      return Object.assign({}, state, {
        typingUsers: state.typingUsers.filter(typingUser => {
            typingUser.name !== action.typingUser.name
        })
      })
    default: return state
  }
}

export default MessageReducer
