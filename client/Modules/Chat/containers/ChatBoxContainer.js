import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'

import ChatBox from '../components/ChatBox'
import { fetchMessages, addMessageRequest, typing, stopTyping } from '../ChatBoxActions'

class ChatBoxContainer extends Component {
	constructor(props) {
		super(props)
		this.socket = props.socket
		this.username = 'test'
		this.handleChange = this.handleChange.bind(this)
		this.handleSendMessage = this.handleSendMessage.bind(this)
		this.state = {
			typingMesssage: '',
			isTyping: false
		}
	}

	componentWillMount = () => {
		const { dispatch, currentChannel } = this.props
		dispatch(fetchMessages(currentChannel))
	}

	componentDidMount = () => {
		const { dispatch } = this.props
		this.socket.on('typing bc', (data) => {
			dispatch(typing(data))
		})
		this.socket.on('stop typing bc', (data) => {
			dispatch(stopTyping(data))
		})
	}

	handleChange = (e) => {
		const { currentChannel } = this.props
		this.setState({
			typingMesssage: e.target.value
		})
		if(e.target.value.length > 0 && !this.state.isTyping) {
			this.socket.emit('typing', {
				name: this.username,
				channel: currentChannel
			})
			this.setState({
				isTyping: true
			})
		}
		if(e.target.value.length === 0 && this.state.isTyping) {
			this.socket.emit('stop typing', {
				name: this.username,
				channel: currentChannel
			})
			this.setState({
				isTyping: false
			})
		}
	}

	handleSendMessage = (e) => {
		const { dispatch, currentChannel } = this.props
		if(e.which == 13) {
			e.preventDefault()
			const message = {
				channel: currentChannel,
				username: this.username,
				text: e.target.value.trim(),
			}
			this.socket.emit('stop typing', {
				user: this.username,
				channel: currentChannel }
			)
			this.setState({
				typingMesssage: '',
				isTyping: false
			})
			if(message.text.length == 0) {
				return
			}
			this.socket.emit('new message', Object.assign({}, message, { time: Date.now()}))
			dispatch(addMessageRequest(message))
		}
	}

	render() {
		const { messages, typingUsers } = this.props

		return (
			<ChatBox messages={messages}
				typingUsers={typingUsers}
				handleChange={this.handleChange}
				handleSendMessage={this.handleSendMessage}
				typingMesssage={this.state.typingMesssage}
				/>
		)
	}
}

ChatBoxContainer.propTypes = {
	socket: PropTypes.object.isRequired,
	typingUsers: PropTypes.arrayOf(PropTypes.shape({
		username: PropTypes.string.isRequired,
		channel: PropTypes.string.isRequire
	})).isRequired,
	messages: PropTypes.arrayOf(PropTypes.shape({
		username: PropTypes.string.isRequired,
		channel: PropTypes.string.isRequired,
		text: PropTypes.string.isRequired,
		time: PropTypes.string.isRequired
	}))
}

const mapStateToProps = (state) => {
	return {
		messages: state.chatBox.messages,
		typingUsers: state.chatBox.typingUsers,
		currentChannel: state.channel.currentChannel
	}
}

export default connect(mapStateToProps)(ChatBoxContainer)
