import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'

import Channel from '../components/Channel'
import { addChannelRequest, changeChannel, fetchChannels } from '../ChannelActions'
import { fetchMessages } from '../../Chat/ChatBoxActions'

class ChannelContainer extends Component {
  constructor(props) {
    super(props)
    this.socket = props.socket
    this.handleChange = this.handleChange.bind(this)
    this.handleAddChannel = this.handleAddChannel.bind(this)
    this.handleChangeChanel = this.handleChangeChanel.bind(this)
    this.state = {
      typingChannel: ''
    }
  }

  componentDidMount = () => {
    const { dispatch } = this.props
    dispatch(fetchChannels())
  }

  handleChange = (e) => {
    this.setState({
      typingChannel: e.target.value
    })
  }

  handleAddChannel = (e) => {
    const { dispatch } = this.props
    if(e.which == 13) {
      e.preventDefault()
      const channel = {
        name: e.target.value.trim(),
      }
      this.setState({
        typingChannel: ''
      })
      if(channel.name.length == 0) {
        return
      }
      dispatch(addChannelRequest(channel))
    }
  }

  handleChangeChanel = (e) => {
    const { dispatch } = this.props
    dispatch(changeChannel(e.target.innerHTML))
    dispatch(fetchMessages(e.target.innerHTML))
  }

  render() {
    const { currentChannel, channels } = this.props
    return (
      <Channel
        currentChannel={currentChannel}
        channels={channels}
        typingChannel={this.state.typingChannel}
        handleChange={this.handleChange}
        handleAddChannel={this.handleAddChannel}
        handleChangeChanel={this.handleChangeChanel}
        />
    )
  }
}
ChannelContainer.propTypes = {
	socket: PropTypes.object.isRequired,
  currentChannel: PropTypes.string.isRequired,
  channels: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired
  })).isRequired
}

const mapStateToProps = (state) => {
  return {
    currentChannel: state.channel.currentChannel,
    channels: state.channel.channels
  }
}

export default connect(mapStateToProps)(ChannelContainer)
