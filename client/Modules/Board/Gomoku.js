class Gomoku {
  constructor(size, cellValue) {
    this.size = size
    this.range = 1,
    this.cellValue = cellValue
    this.patterns = {
      line5: [[cellValue,cellValue,cellValue,cellValue,cellValue]],
      line4: [[0, cellValue, cellValue, cellValue, cellValue, 0]],
      line4blocked: [
        [-cellValue, cellValue, 0, cellValue, cellValue, cellValue], [-cellValue, cellValue, cellValue, 0, cellValue, cellValue],
        [-cellValue, cellValue, cellValue, cellValue, 0, cellValue], [-cellValue, cellValue, cellValue, cellValue, cellValue, 0],
        [0, cellValue, cellValue, cellValue, cellValue, -cellValue], [cellValue, 0, cellValue, cellValue, cellValue, -cellValue],
        [cellValue, cellValue, 0, cellValue, cellValue, -cellValue], [cellValue, cellValue, cellValue, 0, cellValue, -cellValue]
      ],
      line3: [
        [0, cellValue, cellValue, cellValue, 0, 0], [0, 0, cellValue, cellValue, cellValue, 0],
        [0, cellValue, 0, cellValue, cellValue, 0], [0, cellValue, cellValue, 0, cellValue, 0]
      ],
      line3blocked: [
        [-cellValue, cellValue, cellValue, cellValue, 0, 0], [-cellValue, cellValue, cellValue, 0, cellValue, 0],
        [-cellValue, cellValue, 0, cellValue, cellValue, 0], [0, 0, cellValue, cellValue, cellValue, -cellValue],
        [0, cellValue, 0, cellValue, cellValue, -cellValue], [0, cellValue, cellValue, 0, cellValue, -cellValue],
        [-cellValue, cellValue, 0, cellValue, 0, cellValue, -cellValue], [-cellValue, 0, cellValue, cellValue, cellValue, 0, -cellValue],
        [-cellValue, cellValue, cellValue, 0, 0, cellValue, -cellValue], [-cellValue, cellValue, 0, 0, cellValue, cellValue, -cellValue]
      ],
      line2: [
        [0, 0, cellValue, cellValue, 0, 0], [0, cellValue, 0, cellValue, 0, 0],
        [0, 0, cellValue, 0, cellValue, 0], [0, cellValue, cellValue, 0, 0, 0],
        [0, 0, 0, cellValue, cellValue, 0], [0, cellValue, 0, 0, cellValue, 0]
      ]
    }
    for(let pattern in this.patterns)  {
      this.createOppositePatern(this.patterns[pattern]).map((oppositePattern) => {
        this.patterns[pattern].push(oppositePattern)
      })
    }
  }
  createOppositePatern = (patterns) => {
    let oppositePatterns = []
    patterns.map((pattern) => {
      let tmp = Array.from(pattern)
      oppositePatterns.push(Array.from(tmp, x => x*-1))
    })
    return oppositePatterns
  }
  evaluateValue = ({line5count, line4count, line3count, line2count, line4blockedCount, line3blockedCount}) => {
      if (line5count > 0)             return 1000000000;
        if (line4count > 0)           return 100000000;
        if (line4blockedCount > 1)           return 10000000;
        if (line3count > 0 && line4blockedCount > 0) return 1000000;
        if (line3count > 1)           return 100000;

        if (line3count == 1) {
          if (line2count == 3)        return 40000;
          if (line2count == 2)        return 38000;
          if (line2count == 1)        return 35000;
          return 3450;
        }

        if (line4blockedCount == 1) {
          if (line2count == 3)        return 4500;
          if (line2count == 2)        return 4200;
          if (line2count == 1)        return 4100;
          return 4050;
        }

        if (line3blockedCount == 1) {
          if (line2count == 3)        return 3400;
          if (line2count == 2)        return 3300;
          if (line2count == 1)        return 3100;
        }

        if (line3blockedCount == 2) {
          if (line2count == 2)        return 3000;
          if (line2count == 1)        return 2900;
        }

        if (line3blockedCount == 3) {
          if (line2count == 1)        return 2800;
        }

        if (line2count == 4)          return 2700;
        if (line2count == 3)          return 2500;
        if (line2count == 2)          return 2000;
        if (line2count == 1)          return 1000;
    return 0;
  }

  evaluateDirection = (array) => {
    return {
      line5count: this.findArray(this.patterns.line5, array),
      line4count: this.findArray(this.patterns.line4, array),
      line3count: this.findArray(this.patterns.line3, array),
      line2count: this.findArray(this.patterns.line2, array),
      line4blockedCount: this.findArray(this.patterns.line4blocked, array),
      line3blockedCount: this.findArray(this.patterns.line3blocked, array),
    }
  }

  evaluatePosition = (topLeftToBottomRight, topRightToBottomLeft, topBottom, leftRight) => {
    let e1 = this.evaluateDirection(topLeftToBottomRight)
    let e2 = this.evaluateDirection(topRightToBottomLeft)
    let e3 = this.evaluateDirection(topBottom)
    let e4 = this.evaluateDirection(leftRight)

    const result = {
      line5count: e1.line5count + e2.line5count + e3.line5count + e4.line5count,
      line4count: e1.line4count + e2.line4count + e3.line4count + e4.line4count,
      line3count: e1.line3count + e2.line3count + e3.line3count + e4.line3count,
      line2count: e1.line2count + e2.line2count + e3.line2count + e4.line2count,
      line4blockedCount: e1.line4blockedCount + e2.line4blockedCount + e3.line4blockedCount + e4.line4blockedCount,
      line3blockedCount: e1.line3blockedCount + e2.line3blockedCount + e3.line3blockedCount + e4.line3blockedCount,
    }

    return this.evaluateValue(result)
  }

  findArray = (subArrays, largerArray) => {
    let existCount = 0
    subArrays.map((subArray) => {
      for(let i=0; i<largerArray.length - subArray.length + 1; i++) {
        if(largerArray[i] == subArray[0]) {
          let subArrayFound = true
          for(let j=0; j<subArray.length; j++) {
            if(subArray[j] != largerArray[i+j]) {
              subArrayFound = false
              break
            }
          }
          if(subArrayFound) {
            existCount++
          }
        }
      }
    })
    return existCount
  }

  /**
   * AI calculate next move
   */
  getBestMove = (prevMatrix, pos) => {
    const possibleMoves = this.getPossibleMoves(prevMatrix, -1)
    const bestMoves = []
    let maxMoveValue = Number.MIN_VALUE

    possibleMoves.map((possibleMove, i) => {
      let moveValue = this.miniMax(possibleMove, 0, 1, prevMatrix)
      if(maxMoveValue == moveValue) {
        bestMoves.push(i)
      }
      if(maxMoveValue < moveValue) {
        maxMoveValue = moveValue
        bestMoves.splice(0)
        bestMoves.push(i)
      }
    })
    return possibleMoves[bestMoves[Math.floor(Math.random() * (bestMoves.length -1 - 0) + 0)]].pos
  }

  getPossibleMoves = (prevMatrix, player) => {
    let possibleMoves = []
    let possibleNextPos = []
    let usedPos = []

    prevMatrix.map((row, i) => {
      row.map((col, j) => {
        col != 0 ? usedPos.push({x: i, y: j}) : null
      })
    })
    usedPos.map((pos) => {
      for(let i=pos.x - this.range; i<=pos.x + this.range; i++) {
        for(let j=pos.y - this.range; j<= pos.y + this.range; j++) {
          if(i>=0 && j>=0 && i<this.size && j<this.size) {
            if(prevMatrix[i][j] == 0) {
              let pos = {x: i, y: j}
              let filtered = possibleNextPos.filter((nextPos) => {
                return nextPos.x == pos.x && nextPos.y == pos.y
              })
              if(filtered.length == 0) {
                possibleNextPos.push(pos)
              }
            }
          }
        }
      }
    })
    possibleNextPos.map((nextPos) => {
      let nextMatrix = []
      for(let i=0; i<this.size; i++) {
        nextMatrix[i] = Array.from(prevMatrix[i])
      }
      nextMatrix[nextPos.x][nextPos.y] = player
      possibleMoves.push({
        matrix: nextMatrix,
        pos: nextPos
      })
    })

    return possibleMoves
  }

  miniMax = (nextMove, depth, player, prevMatrix) => {
    if(depth == 0) return  this.heuristic(nextMove, prevMatrix)
    // implement complexity
  }

  getLineByPos = (matrix, x, y, dx, dy) => {
    let line = []
    line.push(matrix[x][y])
    for(let i=1; i<5; i++) {
      let nextX = x - dx*i
      let nextY = y - dy*i
      if(nextX >= this.size || nextY >= this.size || nextX < 0 || nextY < 0) break
      line.unshift(matrix[nextX][nextY])
      // if(matrix[nextX][nextY] == -this.cellValue) {
      //   break;
      // }
    }
    for(let i=1; i<5; i++) {
      let prevX = x + dx*i
      let prevY = y + dy*i
      if(prevX >= this.size || prevY >= this.size || prevX < 0 || prevY < 0) break
      line.push(matrix[prevX][prevY])
      // if(matrix[prevX][prevY] == -this.cellValue) {
      //   break;
      // }
    }
    return line
  }

  heuristic = (nextMove, prevMatrix) => {
    const predictMoveValue = this.evaluatePosition(
      this.getLineByPos(nextMove.matrix, nextMove.pos.x, nextMove.pos.y, 1, 0),
      this.getLineByPos(nextMove.matrix, nextMove.pos.x, nextMove.pos.y, 0, 1),
      this.getLineByPos(nextMove.matrix, nextMove.pos.x, nextMove.pos.y, 1, 1),
      this.getLineByPos(nextMove.matrix, nextMove.pos.x, nextMove.pos.y, 1, -1)
    )
    nextMove.matrix[nextMove.pos.x][nextMove.pos.y] = -this.cellValue

    const predictOppositeMoveValue = this.evaluatePosition(
      this.getLineByPos(nextMove.matrix, nextMove.pos.x, nextMove.pos.y, 1, 0),
      this.getLineByPos(nextMove.matrix, nextMove.pos.x, nextMove.pos.y, 0, 1),
      this.getLineByPos(nextMove.matrix, nextMove.pos.x, nextMove.pos.y, 1, 1),
      this.getLineByPos(nextMove.matrix, nextMove.pos.x, nextMove.pos.y, 1, -1)
    )

    return 2*predictMoveValue + predictOppositeMoveValue
  }

  checkWin = (prevMatrix) => {
    let winner = -1
    let dxys = [[1,0], [0,1], [1,1], [1,-1]]
    for(let i=0; i<this.size; i++) {
      for(let j=0; j<this.size; j++) {
        let line5count = 0, line4count = 0
        dxys.map((dxy) => {
          let line = this.getLineByPos(prevMatrix, i, j, dxy[0], dxy[1])
          line5count += this.findArray(this.patterns.line5, line)
          line4count += this.findArray(this.patterns.line4, line)
        })
        if(line5count > 0 || line4count > 0) {
          winner = prevMatrix[i][j]
        }
      }
    }

    return winner
  }
}

export default Gomoku
