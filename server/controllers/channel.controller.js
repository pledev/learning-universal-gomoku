import Channel from '../models/channel';

export function getChannels(req, res) {
  Channel.find({}).exec((err, channels) => {
    if (err) {
      res.status(500).send(err);
    }
    res.json({ channels });
  });
}

export function addChannel(req, res) {
  if(!req.body.channel.name) {
    res.status(403).end();
  }

  const newChannel = new Channel(req.body.channel);

  newChannel.save((err, saved) => {
    if (err) {
      res.status(500).send(err);
    }
    res.json({ channel: saved });
  });
}
