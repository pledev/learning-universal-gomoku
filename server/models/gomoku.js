import mongoose from 'mongoose'
const Schema = mongoose.Schema

const gomokuSchema = new Schema({
  board: Schema.Types.Mixed,
  channel: { type: 'String', required: true},
  player1: { type: 'String', required: true},
  player2: { type: 'String' },
  winner: { type: 'String' },
  time: { type: 'Date', default: Date.now, required: true }
}, {
  collection: 'gomoku'
})

export default mongoose.model('Gomoku', gomokuSchema)
