import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import Helmet from 'react-helmet'
import HeaderContainer from '../Modules/Partials/containers/HeaderContainer'

const App = ({ children }) => {
  return (
    <div>
      <Helmet
        title="Gomoku"
        titleTemplate="%s - React Redux Isomorphism Learning Boilerplate"
        meta={[
          { charset: 'utf-8' },
          { 'http-equiv': 'X-UA-Compatible',content: 'IE=edge' },
          { name: 'viewport', content: 'width=device-width, initial-scale=1' },
          { name: 'description', content: 'Learning React Redux'},
          { name: 'author', content: 'p-le'}
        ]}
        link={[
          { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/bulma/0.1.2/css/bulma.css', type: 'text/css'},
          { rel: 'stylesheet', href: 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css', type: 'text/css'}
        ]}
      />
      <HeaderContainer />
      {children}
    </div>
  )
}

App.propTypes = {
  children: PropTypes.object
}

export default connect()(App)
