import React from 'react'
import { Route, IndexRoute } from 'react-router'

import App from '../client/App/App'
import GomokuPage from '../client/Page/GomokuPage'
import ChatPage from '../client/Page/ChatPage'
import HomePage from '../client/Page/HomePage'

export default (
  <Route path="/" component={App}>
    <IndexRoute component={HomePage} />
    <Route path="gomoku" component={GomokuPage} />
    <Route path="chat" component={ChatPage} />
  </Route>
)
