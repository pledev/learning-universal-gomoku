import React, { PropTypes } from 'react'

const Channel = ({ currentChannel, typingChannel, channels, handleChange, handleAddChannel, handleChangeChanel }) => {
  return (
    <aside className="menu">
      <input className="input" type="text" value={typingChannel}
        placeholder="Add Channel"
        onChange={handleChange}
        onKeyDown={handleAddChannel}/>
      <p className="menu-label">Channels</p>
      <ul className="menu-list">
        {
          channels.map((channel, index) => {
            return <li key={index} onClick={handleChangeChanel}>
              <a className={currentChannel === channel.name ? 'is-active' : null}>{channel.name}</a>
            </li>
          })
        }
      </ul>
    </aside>
  )
}

export default Channel
