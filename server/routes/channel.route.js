import { Router } from 'express'
import * as ChannelController from '../controllers/channel.controller'

const router = new Router()

router.route('/channels').get(ChannelController.getChannels)
router.route('/channels').post(ChannelController.addChannel)

export default router
