import { combineReducers } from 'redux'
import BoardReducer from '../client/Modules/Board/BoardReducer'
import ChatBoxReducer from '../client/Modules/Chat/ChatBoxReducer'
import ChannelReducer from '../client/Modules/Channel/ChannelReducer'

const rootReducer = combineReducers({
  board: BoardReducer,
  chatBox: ChatBoxReducer,
  channel: ChannelReducer
})

export default rootReducer
