import callApi from '../../../common/util'

export const ADD_CHANNEL = 'ADD_CHANNEL'
export const CHANGE_CHANNEL = 'CHANGE_CHANNEL'
export const REQUEST_CHANNELS = 'REQUEST_CHANNELS'
export const RECEIVE_CHANNELS = 'RECEIVE_CHANNELS'

export const fetchChannels = () => {
  return dispatch => {
    dispatch(requestChannels())
    return callApi('channels').then(res => {
      dispatch(receiveChannels(res.channels))
    }).catch(error => {throw error})
  }
}

function requestChannels() {
  return {
    type: REQUEST_CHANNELS
  }
}

function receiveChannels(channels) {
  return {
    type: RECEIVE_CHANNELS,
    channels
  }
}

const addChannel = (channel) => {
  return {
    type: ADD_CHANNEL,
    channel
  };
}

export const addChannelRequest = (channel) => {
  return (dispatch) => {
    return callApi('channels', 'post', {
      channel: channel
    }).then(res => dispatch(addChannel(res.channel)))
  }
}

export const changeChannel = (channel) => {
  return {
    type: CHANGE_CHANNEL,
    channel
  }
}
