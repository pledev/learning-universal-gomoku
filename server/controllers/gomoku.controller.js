import Gomoku from '../models/gomoku';

export function getGame(req, res) {
  Gomoku.findOne({channel: req.params.channel}).sort({time: -1}).exec((err, game) => {
    if (err) {
      res.status(500).send(err);
    }
    res.json({ game });
  });
}

export function updateGame(req, res) {
  let board = req.body.oldBoard
  let pos = req.body.pos
  let value = req.body.value
  
  board[pos.x][pos.y] = value

  Gomoku.findOneAndUpdate({_id: req.body.id}, {board: board}, {new: true}, (err, updated) => {
    if (err) {
      return res.status(500).send(err);
    }
    res.json({
      pos: pos
    })
  });
}

export function createGame(req, res) {
  if(!req.body.username) {
    res.status(403).end()
  }
  const board = Array.apply(0, Array(15)).map(() => {
    return Array.apply(0, Array(15).fill(0))
  })
  const newGame = new Gomoku({
    board: board,
    channel: req.params.channel,
    player1: req.body.username,
    player2: '',
  })

  newGame.save((err, saved) => {
    if(err) {
      res.status(500).send(err)
    } else {
      res.json({ game: saved })
    }
  })
}
