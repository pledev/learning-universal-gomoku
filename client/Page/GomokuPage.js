import React, { Component } from 'react'
import io from 'socket.io-client'

import BoardContainer from '../Modules/Board/containers/BoardContainer'
import ChannelContainer from '../Modules/Channel/containers/ChannelContainer'
import ChatBoxContainer from '../Modules/Chat/containers/ChatBoxContainer'

class GomokuPage extends Component {
  constructor(props) {
    super(props)
    this.socket = io('', { path: '/gomoku' })
  }

  render() {
    return (
      <div className="columns" style={{marginTop: '20px'}}>
        <div className="column is-3">
          <ChatBoxContainer socket={this.socket}/>
        </div>
        <div className="column is-6">
          <BoardContainer socket={this.socket}/>
        </div>
        <div className="column is-3">
          <ChannelContainer socket={this.socket}/>
        </div>
      </div>
    )
  }
}

export default GomokuPage
