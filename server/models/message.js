import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const messageSchema = new Schema({
  username: { type: 'String', required: true },
  channel: { type: 'String', required: true },
  text: { type: 'String', required: true },
  time: { type: 'Date', default: Date.now, required: true }
});

export default mongoose.model('Message', messageSchema);
