import callApi from '../../../common/util'

export const SET_NAME = 'SET_NAME'
export const REQUEST_GAME = 'REQUEST_GAME'
export const RECEIVE_GAME = 'RECEIVE_GAME'
export const CREATE_GAME = 'CREATE_GAME'
export const START_GAME = 'START_GAME'
export const MAKE_MOVE = 'MAKE_MOVE'
export const MAKE_ANSWER = 'MAKE_ANSWER'
export const CHOOSE_GAME_TYPE = 'CHOOSE_GAME_TYPE'

export const setName = (username) => {
  return {
    type: SET_NAME,
    username
  }
}

export const startGame = () => {
  return {
    type: START_GAME
  }
}

export const makeMove = (pos, isNextTurn, value) => {
  return {
    type: MAKE_MOVE,
    pos: pos,
    isNextTurn: isNextTurn,
    value: value
  }
}
export const makeMoveRequest = (data) => {
  return dispatch => {
    dispatch(makeMove(data.pos, false, data.value))
    return callApi('gomoku/move', 'post', {
      id: data.id,
      oldBoard: data.oldBoard,
      pos: data.pos,
      value: data.value
    }).catch(error => {throw error})
  }
}

export const chooseGameType = (typeId) => {
  return {
    type: CHOOSE_GAME_TYPE,
    typeId
  }
}

export const makeAnswer = (pos) => {
  return {
    type: MAKE_ANSWER,
    pos
  }
}

const requestGame = () => {
  return {
    type: REQUEST_GAME
  }
}
const receiveGame = (game, value) => {
  return {
    type: RECEIVE_GAME,
    game,
    value
  }
}
const createGame = () => {
  return {
    type: CREATE_GAME
  }
}
export const fetchGame = (currentChannel) => {
  console.log('fetch game')
  return dispatch => {
    dispatch(requestGame())
    return callApi(`gomoku/${currentChannel}`).then(res => {
      dispatch(receiveGame(res.game, -1))
    }).catch(error => {throw error})
  }
}

export const createGameRequest = (username, currentChannel) => {
  console.log('create game')
  return dispatch => {
    dispatch(createGame())
    return callApi(`gomoku/${currentChannel}`, 'post', {
      username: username
    }).then(res => {
      dispatch(receiveGame(res.game, 1))
    })
  }
}
