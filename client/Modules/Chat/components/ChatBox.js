import React, { Component, PropTypes } from 'react'

const ChatBox = ({ messages, typingMesssage, typingUsers, handleChange, handleSendMessage }) => {
  const typingList = []
  typingUsers.map((typingUser) => {
    typingList.push(<p>{typingUser.name} is typing ...</p>)
  })
  return (
    <div>
      <div className="box" style={{height: '200px', overflow: 'auto'}}>
      {
        messages.map((message) => <p key={message._id}>{new Date(message.time).toLocaleTimeString()}: {message.text}</p>)
      }
      </div>
      {typingList}
      <p className="control">
        <textarea
          className="textarea"
          autoFocus="true"
          placeholder="Type here to chat!"
          value={typingMesssage}
          onChange={handleChange}
          onKeyDown={handleSendMessage}
          />
      </p>
    </div>
  )
}

ChatBox.propTypes = {
  messages: PropTypes.arrayOf(PropTypes.shape({
    username: PropTypes.string.isRequired,
    channel: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    time: PropTypes.string.isRequired
  })).isRequired
}

export default ChatBox
