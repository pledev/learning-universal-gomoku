import User from '../models/user'

export function addUser(req, res) {
  if(!req.body.user.name) {
    res.status(403).end();
  }

  const newUser = new User(req.body.user);

  newUser.save((err, saved) => {
    if (err) {
      res.status(500).send(err);
    }
    res.json({ channel: saved });
  });
}
