import { Router } from 'express'
import * as MessageController from '../controllers/message.controller'

const router = new Router()

router.route('/messages/?:channel').get(MessageController.getMessages)
router.route('/messages').post(MessageController.addMessage)

export default router
