import { Router } from 'express'
import * as GomokuController from '../controllers/gomoku.controller'

const router = new Router()

router.route('/gomoku/:channel').get(GomokuController.getGame)
router.route('/gomoku/move').post(GomokuController.updateGame)
router.route('/gomoku/:channel').post(GomokuController.createGame)


export default router
