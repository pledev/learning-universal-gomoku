require('babel-polyfill');
import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { Router, browserHistory } from 'react-router'
import routes from '../common/routes'
import configureStore from '../common/store'

const initialState = window.__INITIAL_STATE__
const store = configureStore(initialState)
const rootElement = document.getElementById('root')

render(
  <Provider store={store}>
	  <Router history={browserHistory}>
	    {routes}
	  </Router>
  </Provider>,
  rootElement
)
