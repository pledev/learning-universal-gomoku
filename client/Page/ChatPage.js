import React, { Component } from 'react'
import io from 'socket.io-client'

import ChannelContainer from '../Modules/Channel/containers/ChannelContainer'
import ChatBoxContainer from '../Modules/Chat/containers/ChatBoxContainer'

class ChatPage extends Component {
  constructor(props) {
    super(props)
      this.socket = io('', { path: '/chat' })
  }

  render() {
    return (
      <div className="columns" style={{marginTop: '20px'}}>
        <div className="column is-9">
          <ChatBoxContainer socket={this.socket}/>
        </div>
        <div className="column is-3">
          <ChannelContainer socket={this.socket}/>
        </div>
      </div>
    )
  }
}

export default ChatPage
