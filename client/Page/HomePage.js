import React, { Component } from 'react'
import Home from '../Modules/Home/Home'

class HomePage extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <Home />
    )
  }
}

export default HomePage
